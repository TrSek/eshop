<?php
require_once("./db/config.php");

	echo "<link rel='stylesheet' href='css/edit.css' type='text/css'>";

	// test opravnenia
	if( $user->authorize != UserRight::admin ) {
		$user->disp_info = $localize->text("Nedostatočné oprávnenie");
		return;
	}

    // nema konkretny subor, poskytne zoznam
    if( empty($_GET['fn']))
    {
    	// zoznam vsetkych dostupnych suborov
    	$list = array();
		if ($handle = opendir(DOC_DIRECTORY)) {
    		while (false!==($file_name = readdir($handle)))
    		{
    			if($file_name[0] != '.')
					$list[] = $file_name;
    		}
	    	closedir($handle);
    		sort($list);
		}
    	
    	echo "<table>";
    	foreach ($list as $file_name)
    	{
    		$data = file(DOC_DIRECTORY . $file_name);
    		echo "<tr>";
    		echo "<td><a href='?fn=". DOC_DIRECTORY . $file_name ."'>". $file_name ."</a></td>";
    		echo "<td>". implode(PHP_EOL,array_splice($data,0,5)). "</td>";
    		echo "</tr>";
    	}
    	echo "</table>";
    	return;
    }

    
	$edit_text = "";
    // pracuje uz s konkretnym suborom
    // uklada, alebo vycitava
    if($_POST['edit_text']) {
       file_put_contents($_GET['fn'], $_POST['edit_text']);
       $edit_text = $_POST['edit_text'];
    }
    else {
       if( file_exists($_GET['fn']))
	   	   $edit_text = file_get_contents($_GET['fn']);
    }
?>

<script src="tinymce/tinymce.min.js"></script>
<script>tinymce.init({ 
     selector:'textarea',
     plugins:'save',
     toolbar: ['save | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'],
     setup: function(editor) {
           editor.addMenuItem('save', {
                icon: 'save',
                text: 'Save',
                cmd: 'mceSave',
                context: 'file',
                disabled: true,
                onPostRender: function () {
                    var self = this;
                    editor.on('nodeChange', function() {
                        self.disabled(editor.getParam("save_enablewhendirty", true) && !editor.isDirty());
                    });
                }
            });
    }
});</script>

<h2><?php echo $_GET['fn'];?></h2>
<form method="post">
  <textarea name="edit_text" rows="27"><?php echo $edit_text;?></textarea>
</form>