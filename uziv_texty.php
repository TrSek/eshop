<?php
require_once("./db/config.php");
require_once("./db/mte/mte.php");
$tabledit = new MySQLtabledit();

# uzivatelske texty pre tabulky
# specialne polozky su:
#   tabulka: meno  stlpec: #       - popis tabulky ktory sa zobrazi
#   tabulka: meno  stlpec: #new    - popis tabulky ak sa pridava novy zaznam
#   tabulka: #web  stlpec: "text"  - lokalizovany text ktory sa zobrazuje kdekolvek
#
# menu ma svoju vlastnu lokalizaciu v tabulke menu/menu_en/menu_cz a pod.


if( $user->authorize != UserRight::admin ) {
	$user->disp_info = $localize->text("Nedostatočné oprávnenie");
	return;
}
	
# database settings:
$tabledit->database_connect_quick(_DB_DATABASE, _DB_HOST, _DB_USER, _DB_PASSWORD, $user->language, 'uziv_texty');
$tabledit->do_it( basename(__FILE__));
$tabledit->database_disconnect();
?>
