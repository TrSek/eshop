<?php
require_once("db/config.php");
require_once("db/mte/mte.php");
$tabledit = new MySQLtabledit();

if( $user->authorize != UserRight::admin ) {
	$user->disp_info = $localize->text("Nedostatočné oprávnenie");
	return;
}

# database settings:
$tabledit->database_connect_quick(_DB_DATABASE, _DB_HOST, _DB_USER, _DB_PASSWORD, $user->language, 'admin');
$tabledit->database_connect();
$tabledit->do_it( basename(__FILE__));
$tabledit->database_disconnect();
?>
