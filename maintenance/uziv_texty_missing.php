<?php
require_once("./db/config.php");
require_once("./db/mte/mte.php");
$tbl_uziv_texty = new MySQLtabledit();
$tbl_uziv_texty_missing = new MySQLtabledit();

	# database settings:
	$tbl_uziv_texty->database_connect_quick(_DB_DATABASE, _DB_HOST, _DB_USER, _DB_PASSWORD, $user->language, 'uziv_texty');
	$tbl_uziv_texty_missing->database_connect_quick(_DB_DATABASE, _DB_HOST, _DB_USER, _DB_PASSWORD, $user->language, 'uziv_texty_missing');

	$table_name = '';
	$table_order = 0;
	$counter = 0;
	
	$tbl_uziv_texty_missing->first();
	while (!$tbl_uziv_texty_missing->eof()) {

		$_POST = null;
		$_POST['mte_new_rec'] = "new";

		$key = $tbl_uziv_texty_missing->values['TABLE_NAME'];
		$value = $tbl_uziv_texty_missing->values['COLUMN_NAME'];
		$popis = $tbl_uziv_texty_missing->values['COLUMN_COMMENT'];
			
		# prvy riadok je meno tabulky
		if( $table_name != $key ) {

			$table_order = 1;
			$table_name = $key;

			# zisti ci sa uz nenachadza
			if( empty($tbl_uziv_texty->get_where("(tabulka = $key) and (stlpec='#')"))) {
				$_POST['tabulka'] = $key;
				$_POST['stlpec'] = '#';
				$_POST['popis'] = empty($popis)? $key : $popis;
				$tbl_uziv_texty->save_rec_directly();
				#
				$_POST = null;
				$_POST['mte_new_rec'] = "new";
			}
		}
		
		$_POST['tabulka'] = $key;
		$_POST['stlpec']  = $value;
		$_POST['popis']   = $value;
		$_POST['poradie'] = $table_order++;
		$_POST['zobrazit'] = 1;
		
		# store it
		$tbl_uziv_texty->save_rec_directly();
		$tbl_uziv_texty_missing->next();
		
		# 
		$counter++;
	}

	$tbl_uziv_texty->database_disconnect();
	$tbl_uziv_texty_missing->database_disconnect();
	echo "Adding '$counter' missing columns to uziv_texty";
?>
