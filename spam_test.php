<?php

// globalne premenne mi tu akosi nefunguju tak cez funkcie
function getMaxZakaz()
{
	return 10;
}

function getTextZakaz()
{
	return 
		array(
			"http",
			"www",
			"sex",
			"viagra",
			"tramadol",
			"penis",
			"cialis",
		);
}


// je tam vela nepripustnych slov?
function isSpam($text_test)
{
	if(empty($text_test))
		return 0;

	if( is_array($text_test))
		$text = $text_test;
	else
		$text[] = $text_test;
	
	//
	$poc = 0;
	foreach(getTextZakaz() as $zt)
	{
		foreach($text as $txt)
		{
			$poc = $poc + substr_count(strtoupper($txt), strtoupper($zt));
			// uz dalej nekontroluj
			if( $poc >= getMaxZakaz())
				return true;
		}
	}
	
	return ( $poc >= getMaxZakaz());
}

?>
