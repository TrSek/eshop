<?php
//session_start();
require_once("db/config.php");
require_once("db/mte/mte.php");
$tabledit = new MySQLtabledit();

# database settings:
$tabledit->database_connect_quick(_DB_DATABASE, _DB_HOST, _DB_USER, _DB_PASSWORD, $user->language, 'zakaznik');
$tabledit->database_connect();

switch( $user->authorize ) {
	# admin vladne vsetkym
	case UserRight::admin :
		break;
		
	# konkretny uzivatel moze len seba sameho
	case UserRight::user :
		# novy uzivatel/alebo edituje svoj zaznam
		$_GET['mte_a'] = empty($_REQUEST['new_user'])? 'edit': 'new';
		$_GET['id'] = $user->values['id_zakaznik'];
		break;		
		
	# neprihlaseny pridava noveho
	case UserRight::none :
		if( $_POST['mte_a'] == 'save') 
		{
			if ($tabledit->save_rec_directly()) {
				$user->AuthorizeEmail('zakaznik', $_POST['login'], $_POST['password']);
				$user->disp_info = $localize->text("Na adresu %s1 bol odoslaný autorizačný email.", $_POST['email']);
			}
			else {
				$user->disp_info = $localize->text("Chyba registrácie : "). mysql_error();
			}
			$tabledit->database_disconnect();
			return;
		}
		$_GET['mte_a'] = 'new';
		break;		
}

$tabledit->do_it( basename(__FILE__));
$tabledit->database_disconnect();
?>
