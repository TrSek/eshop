<?php
require_once("./db/config.php");
require_once("./db/mte/mte.php");

class LocalizeApi {

	# language
	var $language = 'sk';
	var $tab_name = '#web';

	# konstruktor
	public function __construct($lang) {
		GLOBAL $languages;
	
		$this->language = $languages[count($languages)-1];
		$this->language = $lang;
		
		# vyber texty z tabulky
		$tabledit = new MySQLtabledit();
		$tabledit->database_connect_quick(_DB_DATABASE, _DB_HOST, _DB_USER, _DB_PASSWORD, $this->language, $this->tab_name);
		$tabledit->database_disconnect();

		# lokalizovane texty		
		$this->texty = $tabledit->show_text;
	}

	# lokalizovany text
	public function text($name, $s1="", $s2="", $s3="") {
		
		$replace = array(
				'%s1' => $s1,
				'%s2' => $s2,
				'%s3' => $s3,
		);

		# male korekcie (odstrani diakritiku a velke pismena)
		$value = $name;
		$name = strtolower($name);
//		setlocale(LC_CTYPE, 'cs_CZ');
//		$name = iconv('UTF-8', 'ASCII//TRANSLIT', $name);
				
		if( isset($this->texty[$name]))
			return str_replace(array_keys($replace), array_values($replace), $this->texty[$name]);
		
		# text nemam takze pridam do tabulky textov
		$this->add_text($name, $value);
		
		# vratim povodne
		return str_replace(array_keys($replace), array_values($replace), $value);
	}
	
	# text nieje v tabulke, pridaj ho do tabulky aj do lokalizacii
	private function add_text($name, $value) {
		GLOBAL $languages;
		
		# odlozime post
		$_POST_old = $_POST;
		$_POST = null;
		
		# pridaj do tabulky
		$tabledit = new MySQLtabledit();
		foreach ($languages as $lang)
		{
			$tabledit->database_connect_quick(_DB_DATABASE, _DB_HOST, _DB_USER, _DB_PASSWORD, $lang, 'uziv_texty');
			# tabulka neni otvorena
			if(( $tabledit->table != 'uziv_texty'.'_'. $lang )
			&& ( $lang != $languages[count($languages)-1]))
				continue;
			#
			$_POST['mte_new_rec'] = "save";
			$_POST['tabulka'] = $this->tab_name;
			$_POST['stlpec'] = $name;
			$_POST['popis'] = $value;
			# uloz
			$answ = $tabledit->save_rec_directly();
			$tabledit->database_disconnect();
		}
		# vratime spat
		$_POST = $_POST_old;
	}
	
	# ake texty chybaju/su naviac medzi lokalizovanymi tabulkami
	public function  lokalize_missing()
	{
		//ALTER TABLE `menu_cz` ADD `pomoc` INT NOT NULL ;
		//ALTER TABLE `menu_cz` DROP `pomoc`;		
	}
	
}
