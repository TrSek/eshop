<?php
require_once("db/config.php");
require_once("db/mte/mte.php");
require_once("fio/fio.api.php");


// reset data, napriklad '2016-01-01 00:00:00'
function transakcie_update_fio($reset_data = '')
{
	$fio = new FioApi(_FIO_KEY);
	$tablebanka = new MySQLtabledit();
	$tabletransaction = new MySQLtabledit();

	date_default_timezone_set('Europe/Prague');
	
	if(!empty($reset_data))
		$fio->reset($reset_data);
	
	$transactions = @$fio->getData();
//	file_put_contents('cache/fio_data.php', serialize($transactions));
//	$transactions = unserialize(file_get_contents('cache/fio_data.php'));

	# database settings:
	$tablebanka->database_connect_quick(_DB_DATABASE, _DB_HOST, _DB_USER, _DB_PASSWORD, 'fio_info');
	$tablebanka->del_rec_all();

	# add to post
	$_POST['mte_new_rec'] = "new";
	$_POST['dateStart'] = $reset_data;
	$_POST['dateEnd'] = Date('Y-m-d, H:i');
	foreach($transactions AS $key => $value) {
		if( !is_array($value))
			$_POST[$key] = $value;
	}
	
	# store it
	$answer = $tablebanka->save_rec_directly();
	$tablebanka->database_disconnect();
	
	#transaction aren't
	if( empty($transactions['transakcia']))
		return;

	# database settings:
	$tabletransaction->database_connect_quick(_DB_DATABASE, _DB_HOST, _DB_USER, _DB_PASSWORD, 'fio_transaction');
	foreach($transactions['transakcia'] AS $transakcia)
	{
		# add to post
		$_POST = null;
		$_POST['mte_new_rec'] = "new";
		foreach($transakcia AS $key => $value) {
			if( !is_array($value))
				$_POST[$key] = $value;
		}
		
		# store it
		$tabletransaction->save_rec_directly();
	}
	
	$tabletransaction->database_disconnect();
}

?>