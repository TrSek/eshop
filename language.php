<?php
require_once("./db/config.php");

function CheckLanguage()
{
	GLOBAL $languages;
	$lang = "";
	
	// vyberieme zo session
	if( !empty($_SESSION['lang']))
		$lang = $_SESSION['lang'];
	
	// prisla zmena v v odkaze
	if( !empty($_REQUEST['lang']))
		$lang = $_REQUEST['lang'];
	
	// nie je v povolenom rozsahu, dam to co je posledne
	if( !in_array($lang, $languages))
		$lang = $languages[ count($languages)-1 ];
	
	// odpoved
	$_SESSION['lang'] = $lang;
	return $lang;
}