<?php
require_once("./db/mte/mte.php");
require_once("./activeMailLib.php");

/**
 * Prava uzivatelov
 * 0 - bez prav - prezeranie
 * 1 - prihlaseny uzivatel
 * 2 - uzivatel v vyssimi pravami
 * 3 - prihlaseny predajca
 * 9 - admin webu
*/
abstract class UserRight
{
	const none      = 0;
	const user      = 1;
	const userHight = 2;
	const company   = 3;
	const admin     = 9;
	const unauthorize=-1;
}

class LoginApi {

	# database settings
	protected $database;
	protected $host;
	protected $user;
	protected $pass;

	# aku uroven autorizacie dosiahol
	var $authorize = UserRight::none;

	# language
	var $language = 'sk';

	# doba vyprsania tiemoutu
	protected $timeout;

	# table of the database
	var $table = array(
	'zakaznik' => array('id_zakaznik', 'authorize', 'email', 'login', 'meno', 'priezvisko'),
	'predajca' => array('id_predajca', 'authorize', 'email', 'login', 'firma'),
	'admin'    => array('id_admin',    'authorize', 'email', 'login', 'meno', 'priezvisko')
	);

	var $values;	// riadok do tabulky v ktorej suhlasi heslo
	var $disp_info = "";

	# konstruktor
	public function __construct($db_name, $host, $user, $pass, $timeout, $lang) {
		$this->database = $db_name;
		$this->host     = $host;
		$this->user     = $user;
		$this->pass     = $pass;
		$this->timeout  = $timeout;
		$this->language = $lang;
	}

	# ake opravenie ziskal
	private function GetAuthorize($tbl, $authorize) {
		# login zatial nieje potvrdeny emailom
		if( !$authorize )       return UserRight::unauthorize;
		if( $tbl == 'zakaznik') return UserRight::user;
		if( $tbl == 'predajca') return UserRight::company;
		if( $tbl == 'admin')    return UserRight::admin;
		return UserRight::none;
	}
	
	# najde kto sa prihlasuje?
	public function CheckPassword($name, $password) {
		# osetrenie vstupov
		$name = htmlentities($name);
		$password = htmlentities($password);
		# nad ktorou tabulkou kontrolujem?
		foreach ($this->table as $tbl => $value) {
			$tablepassw = new MySQLtabledit();
			$tablepassw->database_connect_quick($this->database, $this->host, $this->user, $this->pass, $this->language, $tbl);
			$this->values = $tablepassw->get_sql("SELECT ". @implode(',', $this->table[$tbl])  ." FROM $tbl WHERE (login like '". $name ."') and (password like '".$password ."') LIMIT 1");
			unset($this->values['obchodne_podmienky']);
			$tablepassw->database_disconnect();
			# nasiel zhodu?
			if( count($this->values) > 0) {

				# toto je prihlaseny user
				$this->values['tbl'] = $tbl;
				$this->authorize = $this->GetAuthorize($tbl, $this->values['authorize']);
				return $this->values;
			}
		}

		$this->authorize = UserRight::none;
		return null;
	}

	# do aktualnej tabulky ulozi nahodne generovany sha1
	private function StoreSha1Sald($sha1) {
		$tbl = $this->values['tbl'];
		$tablepassw = new MySQLtabledit();
		$tablepassw->database_connect_quick($this->database, $this->host, $this->user, $this->pass, $this->language, $tbl);

		# odlozime
		$POST_push = $_POST;

		# data pre ulozenie
		$_POST = null;
		$_POST['mte_a'] = "";
		$_POST['sha1_sald'] = $sha1;
		$_POST[$this->table[$tbl][0]] = $this->values[ $this->table[$tbl][0]];

		$tablepassw->save_rec_directly();
		$tablepassw->database_disconnect();
		# vratime spat
		$_POST = $POST_push;
	}

	# odhlasenie
	public function Logout() {
		GLOBAL $localize;
		
		$this->disp_info = $localize->text("Užívateľ je odhlásený.");
		$this->authorize = UserRight::none;
		$this->values;
		$this->SetSession();
	}

	# uloz do session a cookies prihlaseneho uzivatela
	public function SetSession() {

		# nacitame session
		session_start();
		$_SESSION[_LOGIN] = null;

		# nie je odkontrolovany user
		if(( empty($this->values))
		|| ( $this->authorize == UserRight::unauthorize )) {
			SetCookie("sha1", "", Time());
			return;
		}

		# naplnime values
		$values['time_finish'] = time() + $this->timeout;
		$values['time_text']   = Date("d.m.Y, H:i:s", $values['time_finish']);
		$values['tbl']         = $this->values['tbl'];
		if( !empty($this->table[$values['tbl']])) {
			foreach ($this->table[$values['tbl']] as $column) {
				$values[$column] = $this->values[$column];
			}
		}

		# vypocet sha1
		$sha1 = base64_encode(mcrypt_create_iv(20, MCRYPT_DEV_URANDOM));
		$this->StoreSha1Sald($sha1);

		#
		$big_string = $sha1;
		foreach ($values as $key => $value) {
			$big_string .= $value;
		}
		$values['sha1'] = sha1($big_string);
		SetCookie("sha1", sha1($big_string), Time() + SESSION_TIMEOUT);

		# nad ktorou tabulkou kontrolujem?
		foreach ($values as $key => $value) {
			$_SESSION[_LOGIN][$key] = $value;
		}
	}

	# kontrola session na prihlaseneho
	public function CheckSession() {
		# nacitame session
		session_start();
		$this->authorize = UserRight::none;
		$this->values = null;

		# cas vyprsal, ani nekontrolujem sha1
		if( time() > $_SESSION[_LOGIN]['time_finish'])
			return $this->authorize;

		$tbl = $_SESSION[_LOGIN]['tbl'];
		$tablepassw = new MySQLtabledit();
		$tablepassw->database_connect_quick($this->database, $this->host, $this->user, $this->pass, $this->language, $tbl);
		$this->values = $tablepassw->get_sql("SELECT ". @implode(',', $this->table[$tbl])  .", sha1_sald FROM $tbl WHERE (". $this->table[$tbl][0] ." = ". $_SESSION[_LOGIN][$this->table[$tbl][0]] .") LIMIT 1");
		$tablepassw->database_disconnect();

		# nic take nemam v tabulke
		if( empty($this->values))
			return $this->authorize;

		# nieje ani v cookies
		if( $_COOKIE['sha1'] != $_SESSION[_LOGIN]['sha1'])
			return $this->authorize;

		# vypocet sha1
		$this->values['tbl'] = $tbl;
		$big_string  = $this->values['sha1_sald'];
		$big_string .= $_SESSION[_LOGIN]['time_finish'];
		$big_string .= $_SESSION[_LOGIN]['time_text'];
		$big_string .= $_SESSION[_LOGIN]['tbl'];
		foreach ($this->table[$tbl] as $value) {
			$big_string .= $_SESSION[_LOGIN][$value];
		}
		# odstran sald nech sa nepovaluje v globalnych premennych
		unset($this->values['sha1_sald']);

		# sha1 sa zhoduje, je to overena session
		if($_COOKIE['sha1'] == sha1($big_string))
		{
			$this->authorize = $this->GetAuthorize($tbl, $this->values['authorize']);
			return $this->authorize;
		}
			
		# nezhoduje sa - vykopnem ho
		return $this->authorize;
	}

	# skontroluj autorizaciu, ak je v $_POST meno/heslo ulozi session
	function Authorize() {
		GLOBAL $localize;
		
		# reset hesla
		if( isset($_REQUEST['reset'])) {
			$this->ResetPassword();
			return $this->authorize;
		}

		# prisiel authorizovat
		if( !empty($_REQUEST['sha'])) {
			$this->AuthorizeLink($_REQUEST['tbl'], $_REQUEST['id'], $_REQUEST['sha']);
			$_REQUEST = null;
		}

		# odhlasujeme sa
		if( isset($_REQUEST['logout'])) {
			$this->Logout();
			return $this->authorize;
		}

		# prihlasuje sa
		if( !empty($_POST['meno']) &&  !empty($_POST['heslo'])) {
			# kontrola hesla
			$this->CheckPassword($_POST['meno'], $_POST['heslo']);
			# uloz session
			$this->SetSession();
		}
		else {
			# skontroluj session
			$this->CheckSession();
			# znova uloz
			//if( $this->authorize != UserRight::none )
			$this->SetSession();
		}

		if( $this->authorize == UserRight::unauthorize )
			$this->disp_info = $localize->text("%s1 nieje autorizovaný, skontroluj email.", $this->values['login']);

		return $this->authorize;
	}

	# zobrazenie prihlasovacieho dialogu
	function InputPassword() {

		echo ("
				<div>
				<br><br><br>
				<form action='index.php' method='POST' style='margin:0px auto;display:table;'>
				<label>
				<p>Meno</p>
				<input name='meno' type='text'>
				</label><br>
				<label>
				<p>Heslo</p>
				<input name='heslo' type='password'>
				</label><br>
				<label>
				<p>
				<input name='remember_me' type='checkbox'> Zapamatať heslo
				</p>
				</label>
				<div clear=''></div>
				<button style='width:150px;' name='action_login'>Prihlásiť</button>
				<br><br><br>
				<a class='button' href='reset_password'>Zabudnuté heslo?</a>
				</form>
				<style>
				input[type=text], input[type=password]{
				width: 230px;
				}
				</style>
				</div>
				<br><br><br>
				");

	}


	# posli emailom authorizaciu
	function AuthorizeEmail($tbl, $login, $password) {
		GLOBAL $localize;
		
		$tablepassw = new MySQLtabledit();
		$tablepassw->database_connect_quick($this->database, $this->host, $this->user, $this->pass, $this->language, $tbl);
		$this->values = $tablepassw->get_sql("SELECT ". $this->table[$tbl][0] .", email FROM $tbl WHERE (login like '". $login ."') and (password like '".$password ."') LIMIT 1");
		$this->values['tbl'] = $tbl;
		$tablepassw->database_disconnect();

		# vypocet sha1
		$web  = WEB_ADRESA;
		$sha1 = base64_encode(mcrypt_create_iv(20, MCRYPT_DEV_URANDOM));
		$id   = $this->values[$this->table[$tbl][0]];
		$this->StoreSha1Sald($sha1);

//		"Práve ste sa registroval na webe %s1\n Pre ďalšiu prácu musíte potvrdiť registráciu na tomto linku:\n %s2";
		$message = $localize->text("registracia na web %s1, potvrd to linkom %s2", 
		                           "http://$web", 
		                           "http://$web/tbl%3D$tbl%26id%3D$id%26sha%3D$sha1");

		$email = new activeMailLib();
		$email->checkaddr=false;
		$email->From(MAIL_ADMIN);
		$email->To($this->values['email']);
		$email->Subject($localize->text("Authorize mail for %s1", WEB_ADRESA));
		$email->Message(html_entity_decode($message), 'UTF-8');
		$email->Send();
	}


	# chce autorizovat linkom
	function AuthorizeLink($tbl, $id, $sha) {
		GLOBAL $localize;

		$id_name = $this->table[$tbl][0];
		$tablepassw = new MySQLtabledit();
		$tablepassw->database_connect_quick($this->database, $this->host, $this->user, $this->pass, $this->language, $tbl);
		$this->values = $tablepassw->get_sql("SELECT $id_name, login FROM $tbl WHERE ($id_name = $id) and (sha1_sald like '$sha') LIMIT 1");

		if( empty($this->values)) {
			$this->disp_info = $localize->text("Autorizácia %s1 sa nepodarila.", $this->values['login']);
		}
		else {
			$tablepassw->get_sql("UPDATE $tbl SET authorize = 1 WHERE ($id_name = $id)");
			$this->disp_info = $localize->text("Účet %s1 je autorizovaný.", $this->values['login']);
		}

		$tablepassw->database_disconnect();
	}
}
