<?php
require("barcode/barcode.php");

$barcode_prefix = "2016ELAN";
$type     = "C39";
$width    = BCD_DEFAULT_WIDTH;
$height   = BCD_DEFAULT_HEIGHT;
$xres     = BCD_DEFAULT_XRES;
$font     = BCD_DEFAULT_FONT;
$style    = BCS_ALIGN_CENTER | BCS_IMAGE_PNG | BCS_DRAW_TEXT | BCS_STRETCH_TEXT;

?>

<br>
<h1>ELÁN 2016</h1>
<table align='center'>
  <tr>
  <?php 
  for($code = 1; $code<40; $code++)
  {
  	$barcode = $barcode_prefix . $code;
  	echo "<td align='center'>";
  	echo "ELÁN TOUR 2016<br>";
  	echo "Myjava - Fotbalový štadión, 1.8.2016 20:00<br>";
  	echo "<img src='./barcode/image.php?code=".$barcode."&style=".$style."&type=".$type."&width=".$width."&height=".$height."&xres=".$xres."&font=".$font."'>";
  	echo "<br><br></td>";
    
    // zalomime
    if(($code % 3) == 0)
    	echo "</tr><tr>";
  }
  ?>
  </tr>
</table>
