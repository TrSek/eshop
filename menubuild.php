<?php
require_once("db/config.php");
require_once("db/mte/mte.php");
require_once("login/LoginApi.php");


# je pre tohoto uzivatela menu dostupne?
function isAuthorize($authorize, $menu, $lang)
{
	GLOBAL $lang_yes;
	
	if(($menu['ukaz_vsetci']   == $lang_yes[$lang]) && (($authorize==UserRight::none) || ($authorize==UserRight::unauthorize))) return true;
	if(($menu['ukaz_kupujuci'] == $lang_yes[$lang]) && ($authorize==UserRight::user)) return true;
	if(($menu['ukaz_predajca'] == $lang_yes[$lang]) && ($authorize==UserRight::company)) return true;
	if(($menu['ukaz_platiaci'] == $lang_yes[$lang]) && ($authorize==UserRight::userHight)) return true;
	if(($menu['ukaz_admin']    == $lang_yes[$lang]) && ($authorize==UserRight::admin)) return true;
	return false;
}

# zbuduje menu
function menuBuild($id, $authorize, $lang)
{
	GLOBAL $localize;
	
	# database settings:
	$tablemenu = new MySQLtabledit();
	$tablemenu->database_connect_quick(_DB_DATABASE, _DB_HOST, _DB_USER, _DB_PASSWORD, $lang, 'menu');
	$menu_str = "";
	
	$menu = $tablemenu->get_sql("SELECT * FROM $tablemenu->table WHERE (id_main_menu = $id) ORDER by id_order");
	while( !empty($menu))
	{
		if( isAuthorize($authorize, $menu, $lang))
		{
			$menu_str .= "\n<li>";
			$menu_str .= "<a href='". $menu['href'] ."'>". $menu['text_menu'] ."</a>";
			# pridam podmenu
			$menu_str .= menuBuild($menu['id_order'], $authorize, $lang);
			$menu_str .= "\n</li>";
		}
		$menu = $tablemenu->next();
	}
	$tablemenu->database_disconnect();
	
	# vsuvka pre hlavne menu
	if( $id == 0)
	{
		$menu_str .= "\n<li><div Id=Jmeno title='". $localize->text("Prihlásený %s1", $user->values['login']) ."'></div></li>";
	}
	
	if( !empty($menu_str))
	{
		$menu_str = (($id==0)? "<ul id='mainMenu'>": "<ul>")
					. $menu_str ."</ul>";
	}	

	return $menu_str;
}

# zobrazim menu
echo menuBuild(0, $user->authorize, $user->language);
?>