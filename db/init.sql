-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 06, 2016 at 11:31 PM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sekerakeu`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `authorize` tinyint(4) NOT NULL DEFAULT '1',
  `login` varchar(35) NOT NULL,
  `password` varchar(35) NOT NULL,
  `sha1_sald` varchar(28) NOT NULL COMMENT 'Soľ pre posledné prihlásenie',
  `email` varchar(55) NOT NULL,
  `meno` varchar(35) NOT NULL,
  `priezvisko` varchar(35) NOT NULL,
  PRIMARY KEY (`id_admin`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci COMMENT='tabuľka administrátorov' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `authorize`, `login`, `password`, `sha1_sald`, `email`, `meno`, `priezvisko`) VALUES
(1, 1, 'trsek', 'kesrt', 'm2zJozFw3vRpdtoOql0Gc5l1iHM=', 'etrsek@gmail.com', 'Zdeno', 'Sekerák'),
(3, 1, 'admin', 'admin', '1LOd35V7F4PswFWa77dJMHJSfaE=', 'info@sekerak.eu', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `fio_info`
--

CREATE TABLE IF NOT EXISTS `fio_info` (
  `id_transakce` int(11) NOT NULL AUTO_INCREMENT,
  `cislo` int(16) NOT NULL,
  `mena` varchar(3) NOT NULL,
  `IBAN` varchar(35) NOT NULL,
  `kod_banky` varchar(4) NOT NULL,
  `openingBalance` double NOT NULL,
  `closingBalance` double NOT NULL,
  `dateStart` date NOT NULL,
  `dateEnd` date NOT NULL,
  `idFrom` int(12) NOT NULL COMMENT 'číslo prvného pohybu v danom výbere',
  `idTo` int(12) NOT NULL COMMENT 'číslo posledného pohybu v danom výbere',
  PRIMARY KEY (`id_transakce`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci COMMENT='fio banka účet' AUTO_INCREMENT=21 ;

--
-- Dumping data for table `fio_info`
--

INSERT INTO `fio_info` (`id_transakce`, `cislo`, `mena`, `IBAN`, `kod_banky`, `openingBalance`, `closingBalance`, `dateStart`, `dateEnd`, `idFrom`, `idTo`) VALUES
(20, 2147483647, 'CZK', 'CZ3620100000002800739611', '2010', 0, 0, '0000-00-00', '2016-05-30', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fio_transaction`
--

CREATE TABLE IF NOT EXISTS `fio_transaction` (
  `id_pohybu` int(11) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `ciastka` double NOT NULL,
  `mena` text NOT NULL,
  `protiucet` text NOT NULL,
  `nazev_protiuctu` text NOT NULL,
  `banka` int(10) NOT NULL,
  `nazev_banky` text NOT NULL,
  `KS` int(4) NOT NULL,
  `VS` int(10) NOT NULL,
  `SS` int(10) NOT NULL,
  `popis_interni` text NOT NULL,
  `popis` text NOT NULL,
  `typ` text NOT NULL,
  `provedl` text NOT NULL,
  `upresneni` text NOT NULL,
  `komentar` text NOT NULL,
  `BIC` text NOT NULL,
  `ident` int(12) NOT NULL,
  PRIMARY KEY (`id_pohybu`)
) ENGINE=InnoDB  DEFAULT  CHARSET=utf8 COLLATE=utf8_slovak_ci COMMENT='fio banka pohyby na účte' AUTO_INCREMENT=94 ;

--
-- Dumping data for table `fio_transaction`
--

INSERT INTO `fio_transaction` (`id_pohybu`, `datum`, `ciastka`, `mena`, `protiucet`, `nazev_protiuctu`, `banka`, `nazev_banky`, `KS`, `VS`, `SS`, `popis_interni`, `popis`, `typ`, `provedl`, `upresneni`, `komentar`, `BIC`, `ident`) VALUES
(61, '2016-01-09', -1303, 'CZK', '', '', 0, '', 0, 3366, 0, 'Nákup: IKEA Praha CM, Praha, CZ, dne 7.1.2016, částka  1303.00 CZK', '', 'Platba kartou', '', '', '', '', 2147483647),
(62, '2016-01-12', 3500, 'CZK', '670100-2200091647', '', 6210, '', 0, 0, 0, 'ZDENKO SEKERAK', '', 'Bezhotovostní příjem', '', '', '', '', 2147483647),
(63, '2016-01-12', 3631, 'CZK', '670100-2200091647', '', 6210, '', 0, 0, 0, 'ZDENKO SEKERAK', '', 'Bezhotovostní příjem', '', '', '', '', 2147483647),
(64, '2016-01-12', -1000, 'CZK', '', '', 0, '', 0, 3366, 0, 'Výběr z bankomatu: CS, NAMESTI MIRU 925, HERMANUV MEST, CZ, dne 10.1.2016, částka  1000.00 CZK', '', 'Platba kartou', '', '', '', '', 2147483647),
(65, '2016-01-15', -3630.83, 'CZK', '2400781618', '', 2010, '', 0, 0, 0, '', '', 'Platba převodem uvnitř banky', '', '', '', '', 2147483647),
(66, '2016-01-30', -500, 'CZK', '', '', 0, '', 0, 3366, 0, 'Výběr z bankomatu: ATM S6CY0093, PARDUBICE, 00, dne 29.1.2016, částka  500.00 CZK', '', 'Platba kartou', '', '', '', '', 2147483647),
(67, '2016-02-03', -200, 'CZK', '', '', 0, '', 0, 0, 0, '', '', 'Dobití kreditu mobilního telefonu', '', '', '', '', 2147483647),
(68, '2016-02-06', -500, 'CZK', '', '', 0, '', 0, 3366, 0, 'Výběr z bankomatu: ATM S6CY0093, PARDUBICE, 00, dne 5.2.2016, částka  500.00 CZK', '', 'Platba kartou', '', '', '', '', 2147483647),
(69, '2016-02-09', -2690, 'CZK', '', '', 0, '', 0, 3366, 0, 'Nákup: HP TRONIC, PARDUBICE, CZ, dne 5.2.2016, částka  2690.00 CZK', '', 'Platba kartou', '', '', '', '', 2147483647),
(70, '2016-02-11', 3631, 'CZK', '670100-2200091647', '', 6210, '', 0, 0, 0, 'ZDENKO SEKERAK', '', 'Bezhotovostní příjem', '', '', '', '', 2147483647),
(71, '2016-02-12', 5000, 'CZK', '670100-2200091647', '', 6210, '', 0, 0, 0, 'ZDENKO SEKERAK', '', 'Bezhotovostní příjem', '', '', '', '', 2147483647),
(72, '2016-02-15', -3630.83, 'CZK', '2400781618', '', 2010, '', 0, 0, 0, '', '', 'Platba převodem uvnitř banky', '', '', '', '', 2147483647),
(73, '2016-02-26', -1500, 'CZK', '2800190673', '', 2010, '', 0, 0, 0, 'Prevod pre Marku', '', 'Platba převodem uvnitř banky', '', '', '', '', 2147483647),
(74, '2016-03-06', -139, 'CZK', '', '', 0, '', 0, 3366, 0, 'Nákup: DM DROGERIE MARKT   -, PARDUBICE, CZ, dne 4.3.2016, částka  139.00 CZK', '', 'Platba kartou', '', '', '', '', 2147483647),
(75, '2016-03-08', 5000, 'CZK', '670100-2200091647', '', 6210, '', 0, 0, 0, 'ZDENKO SEKERAK', '', 'Bezhotovostní příjem', '', '', '', '', 2147483647),
(76, '2016-03-11', 3631, 'CZK', '670100-2200091647', '', 6210, '', 0, 0, 0, 'ZDENKO SEKERAK', '', 'Bezhotovostní příjem', '', '', '', '', 2147483647),
(77, '2016-03-15', -3630.83, 'CZK', '2400781618', '', 2010, '', 0, 0, 0, '', '', 'Platba převodem uvnitř banky', '', '', '', '', 2147483647),
(78, '2016-03-19', -1000, 'CZK', '', '', 0, '', 0, 3366, 0, 'Výběr z bankomatu: ATM S6CY0093, PARDUBICE, 00, dne 17.3.2016, částka  1000.00 CZK', '', 'Platba kartou', '', '', '', '', 2147483647),
(79, '2016-03-24', -200, 'CZK', '', '', 0, '', 0, 0, 0, '', '', 'Dobití kreditu mobilního telefonu', '', '', '', '', 2147483647),
(80, '2016-04-08', 8298, 'CZK', '670100-2200091647', '', 6210, '', 0, 0, 0, 'ZDENKO SEKERAK', '', 'Bezhotovostní příjem', '', '', '', '', 2147483647),
(81, '2016-04-10', -2298, 'CZK', '', '', 0, '', 0, 3366, 0, 'Nákup: GLOBUS BM 45 PARDUBIC, PARDUBICE, dne 6.4.2016, částka  2298.00 CZK', '', 'Platba kartou', '', '', '', '', 2147483647),
(82, '2016-04-10', -1000, 'CZK', '', '', 0, '', 0, 3366, 0, 'Výběr z bankomatu: CSOB 1269 PARDUBICE, Pardubice, dne 6.4.2016, částka  1000.00 CZK', '', 'Platba kartou', '', '', '', '', 2147483647),
(83, '2016-04-11', 3631, 'CZK', '670100-2200091647', '', 6210, '', 0, 0, 0, 'ZDENKO SEKERAK', '', 'Bezhotovostní příjem', '', '', '', '', 2147483647),
(84, '2016-04-15', -3630.83, 'CZK', '2400781618', '', 2010, '', 0, 0, 0, '', '', 'Platba převodem uvnitř banky', '', '', '', '', 2147483647),
(85, '2016-05-04', -1000, 'CZK', '', '', 0, '', 0, 3366, 0, 'Výběr z bankomatu: ATM S6CY0093, PARDUBICE, 00, dne 2.5.2016, částka  1000.00 CZK', '', 'Platba kartou', '', '', '', '', 2147483647),
(86, '2016-05-06', -2365.3, 'CZK', '', '', 0, '', 0, 3366, 0, 'Nákup: Tesco Hradec Kralove R, Hradec Kralov, CZ, dne 4.5.2016, částka  2365.30 CZK', '', 'Platba kartou', '', '', '', '', 2147483647),
(87, '2016-05-06', -209, 'CZK', '', '', 0, '', 0, 3366, 0, 'Nákup: www.le.cz, PRAHA 3, CZ, dne 4.5.2016, částka  209.00 CZK', '', 'Platba kartou', '', '', '', '', 2147483647),
(88, '2016-05-06', -1000, 'CZK', '', '', 0, '', 0, 3366, 0, 'Výběr z bankomatu: ATM S6CY0023, PARDUBICE, 00, dne 5.5.2016, částka  1000.00 CZK', '', 'Platba kartou', '', '', '', '', 2147483647),
(89, '2016-05-09', 7872, 'CZK', '670100-2200091647', '', 6210, '', 0, 0, 0, 'ZDENKO SEKERAK', '', 'Bezhotovostní příjem', '', '', '', '', 2147483647),
(90, '2016-05-11', 3631, 'CZK', '670100-2200091647', '', 6210, '', 0, 0, 0, 'ZDENKO SEKERAK', '', 'Bezhotovostní příjem', '', '', '', '', 2147483647),
(91, '2016-05-13', -200, 'CZK', '', '', 0, '', 0, 0, 0, '', '', 'Dobití kreditu mobilního telefonu', '', '', '', '', 2147483647),
(92, '2016-05-13', -2000, 'CZK', '', '', 0, '', 0, 3366, 0, 'Výběr z bankomatu: ATM S6CY0093, PARDUBICE, 00, dne 12.5.2016, částka  2000.00 CZK', '', 'Platba kartou', '', '', '', '', 2147483647),
(93, '2016-05-15', -3630.83, 'CZK', '2400781618', '', 2010, '', 0, 0, 0, '', '', 'Platba převodem uvnitř banky', '', '', '', '', 2147483647);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) NOT NULL COMMENT 'Poradie menu',
  `id_main_menu` int(11) NOT NULL COMMENT 'Id na rodičovské menu',
  `text_menu` varchar(35) NOT NULL,
  `href` varchar(35) NOT NULL COMMENT 'Aký príkaz sa má vykonať',
  `ukaz_vsetci` enum('nie','ano') NOT NULL COMMENT 'Menu vidia všetci',
  `ukaz_kupujuci` enum('nie','ano') NOT NULL COMMENT 'Menu vidia prihlásený užívatelia',
  `ukaz_predajca` enum('nie','ano') NOT NULL COMMENT 'Menu vidia predajcovia',
  `ukaz_platiaci` enum('nie','ano') NOT NULL COMMENT 'Menu vidia predajcovia ktorý zaplatili',
  `ukaz_admin` enum('nie','ano') NOT NULL COMMENT 'Menu vidí admin systému',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci COMMENT='Slovenské menu' AUTO_INCREMENT=35 ;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `id_order`, `id_main_menu`, `text_menu`, `href`, `ukaz_vsetci`, `ukaz_kupujuci`, `ukaz_predajca`, `ukaz_platiaci`, `ukaz_admin`) VALUES
( 1,  1,  0, '', '#', 'ano', 'ano', 'ano', 'ano', 'ano'),
( 2, 10,  0, 'Info', 'info.php', 'ano', 'ano', 'ano', 'ano', 'ano'),
( 3, 11, 10, 'Predávajúci', 'documents/predajca.html', 'nie', 'nie', 'ano', 'ano', 'ano'),
( 4, 12, 10, 'Zákazníci', 'documents/zakaznik.html', 'ano', 'ano', 'ano', 'ano', 'ano'),
( 5, 20,  0, 'Cenové hity', '#', 'ano', 'ano', 'ano', 'ano', 'ano'),
( 6, 21, 20, 'Zľavy', '#', 'ano', 'ano', 'ano', 'ano', 'ano'),
( 7, 22, 20, 'Výpredaje', '#', 'nie', 'nie', 'ano', 'ano', 'ano'),
( 8, 23, 20, 'Top ponuky', '#', 'ano', 'ano', 'ano', 'ano', 'ano'),
( 9, 30,  0, 'Novinky', '#', 'ano', 'ano', 'ano', 'ano', 'ano'),
(10, 40,  0, 'Nakupovanie', '#', 'nie', 'nie', 'ano', 'ano', 'ano'),
(11, 50,  0, 'e-Letáky', '#', 'ano', 'ano', 'ano', 'ano', 'ano'),
(12, 60,  0, 'Súťaže', '#', 'nie', 'ano', 'ano', 'ano', 'ano'),
(13, 70,  0, 'Hudobné koncerty', 'koncertny_listok.php', 'ano', 'ano', 'ano', 'ano', 'ano'),
(14, 80,  0, 'Moja ponuka', '#', 'nie', 'nie', 'ano', 'ano', 'ano'),
(15, 81, 80, 'Cenník moj', '#', 'nie', 'nie', 'ano', 'ano', 'ano'),
(16, 82, 80, 'Aktualizovať cenník', '#', 'nie', 'nie', 'ano', 'ano', 'ano'),
(17, 83, 80, 'Import cenníka', '#', 'nie', 'nie', 'ano', 'ano', 'ano'),
(18, 84, 80, 'Aktualizovať ponuku', '#', 'nie', 'nie', 'ano', 'ano', 'ano'),
(19, 90,  0, 'Administrácia', '', 'nie', 'nie', 'nie', 'nie', 'ano'),
(20, 91, 90, 'Menu', 'menu.php', 'nie', 'nie', 'nie', 'nie', 'ano'),
(21, 92, 90, 'Uživatelské texty', 'uziv_texty.php', 'nie', 'nie', 'nie', 'nie', 'ano'),
(22, 93, 90, 'Predajci', 'predajca.php', 'nie', 'nie', 'nie', 'nie', 'ano'),
(23, 94, 90, 'Zákazníci', 'zakaznik.php', 'nie', 'nie', 'nie', 'nie', 'ano'),
(24, 95, 90, 'Platby prijaté', 'platby.php', 'nie', 'nie', 'nie', 'nie', 'ano'),
(25, 97, 90, 'Administrátori', 'admin.php', 'nie', 'nie', 'nie', 'nie', 'ano'),
(26, 98, 90, 'Dokumenty', 'edit.php', 'nie', 'nie', 'nie', 'nie', 'ano'),
(27, 100, 0, 'Prihlásenie', 'login.php', 'ano', 'ano', 'ano', 'ano', 'ano'),
(28, 101, 100, 'Prihlásenie', 'login.php', 'ano', 'nie', 'nie', 'nie', 'nie'),
(29, 102, 100, 'Registrácia zákazníci', 'zakaznik.php?new', 'ano', 'nie', 'nie', 'nie', 'ano'),
(30, 103, 100, 'Registrácia podnikatelia', 'predajca.php?new', 'ano', 'nie', 'nie', 'nie', 'ano'),
(31, 104, 100, 'Podnikatelia - platené služby', '#', 'nie', 'nie', 'nie', 'ano', 'ano'),
(32, 105, 100, 'Podnikatelia - moja ponuka', '#', 'nie', 'nie', 'ano', 'ano', 'ano'),
(33, 107, 100, 'Zabudnuté heslo', 'login.php?reset', 'ano', 'nie', 'nie', 'nie', 'nie'),
(34, 110, 100, 'Odhlásiť sa', 'login.php?logout', 'nie', 'ano', 'ano', 'ano', 'ano');

-- --------------------------------------------------------

--
-- Table structure for table `menu_cz`
--

CREATE TABLE IF NOT EXISTS `menu_cz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) NOT NULL COMMENT 'Pořadí menu',
  `id_main_menu` int(11) NOT NULL COMMENT 'Id na rodičovské menu',
  `text_menu` varchar(35) NOT NULL,
  `href` varchar(35) NOT NULL COMMENT 'Příkaz',
  `ukaz_vsetci` enum('ne','ano') NOT NULL COMMENT 'Menu pro všechny',
  `ukaz_kupujuci` enum('ne','ano') NOT NULL COMMENT 'Menu pro přihlášené užívatele',
  `ukaz_predajca` enum('ne','ano') NOT NULL COMMENT 'Menu pro firmy',
  `ukaz_platiaci` enum('ne','ano') NOT NULL COMMENT 'Menu pro platící firmy',
  `ukaz_admin` enum('ne','ano') NOT NULL COMMENT 'Menu pro administrátori',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci COMMENT='České menu' AUTO_INCREMENT=35 ;

--
-- Dumping data for table `menu_cz`
--

INSERT INTO `menu_cz` (`id`, `id_order`, `id_main_menu`, `text_menu`, `href`, `ukaz_vsetci`, `ukaz_kupujuci`, `ukaz_predajca`, `ukaz_platiaci`, `ukaz_admin`) VALUES
( 1,  1,  0, '', '#', 'ano', 'ano', 'ano', 'ano', 'ano'),
( 2, 10,  0, 'Info', 'info.php', 'ano', 'ano', 'ano', 'ano', 'ano'),
( 3, 11, 10, 'Prodávajíci', 'documents/predajca.html', 'ne', 'ne', 'ano', 'ano', 'ano'),
( 4, 12, 10, 'Zákazníci', 'documents/zakaznik.html', 'ano', 'ano', 'ano', 'ano', 'ano'),
( 5, 20,  0, 'Cenové hity', '#', 'ano', 'ano', 'ano', 'ano', 'ano'),
( 6, 21, 20, 'Slevy', '#', 'ano', 'ano', 'ano', 'ano', 'ano'),
( 7, 22, 20, 'Výprodeje', '#', 'ne', 'ne', 'ano', 'ano', 'ano'),
( 8, 23, 20, 'Top ponuky', '#', 'ano', 'ano', 'ano', 'ano', 'ano'),
( 9, 30,  0, 'Novinky', '#', 'ano', 'ano', 'ano', 'ano', 'ano'),
(10, 40,  0, 'Nakupování', '#', 'ne', 'ne', 'ano', 'ano', 'ano'),
(11, 50,  0, 'e-Letáky', '#', 'ano', 'ano', 'ano', 'ano', 'ano'),
(12, 60,  0, 'Soutěže', '#', 'ne', 'ano', 'ano', 'ano', 'ano'),
(13, 70,  0, 'Hudební koncerty', 'koncertny_listok.php', 'ano', 'ano', 'ano', 'ano', 'ano'),
(14, 80,  0, 'Moje ponuka', '#', 'ne', 'ne', 'ano', 'ano', 'ano'),
(15, 81, 80, 'Ceník můj', '#', 'ne', 'ne', 'ano', 'ano', 'ano'),
(16, 82, 80, 'Aktualizovat ceník', '#', 'ne', 'ne', 'ano', 'ano', 'ano'),
(17, 83, 80, 'Import ceníka', '#', 'ne', 'ne', 'ano', 'ano', 'ano'),
(18, 84, 80, 'Aktualizovat ponuku', '#', 'ne', 'ne', 'ano', 'ano', 'ano'),
(19, 90,  0, 'Administrace', '', 'ne', 'ne', 'ne', 'ne', 'ano'),
(20, 91, 90, 'Menu', 'menu.php', 'ne', 'ne', 'ne', 'ne', 'ano'),
(21, 92, 90, 'Uživatelské texty', 'uziv_texty.php', 'ne', 'ne', 'ne', 'ne', 'ano'),
(22, 93, 90, 'Prodajci', 'predajca.php', 'ne', 'ne', 'ne', 'ne', 'ano'),
(23, 94, 90, 'Zákazníci', 'zakaznik.php', 'ne', 'ne', 'ne', 'ne', 'ano'),
(24, 95, 90, 'Platby přijatý', 'platby.php', 'ne', 'ne', 'ne', 'ne', 'ano'),
(25, 97, 90, 'Administrátoři', 'admin.php', 'ne', 'ne', 'ne', 'ne', 'ano'),
(26, 98, 90, 'Dokumenty', 'edit.php', 'ne', 'ne', 'ne', 'ne', 'ano'),
(27, 100, 0, 'Přihlášení', 'login.php', 'ano', 'ano', 'ano', 'ano', 'ano'),
(28, 101, 100, 'Přihlášení', 'login.php', 'ano', 'ne', 'ne', 'ne', 'ne'),
(29, 102, 100, 'Registrace zákazníci', 'zakaznik.php?new', 'ano', 'ne', 'ne', 'ne', 'ano'),
(30, 103, 100, 'Registrace podnikatelé', 'predajca.php?new', 'ano', 'ne', 'ne', 'ne', 'ano'),
(31, 104, 100, 'Podnikatelé - placené služby', '#', 'ne', 'ne', 'ne', 'ano', 'ano'),
(32, 105, 100, 'Podnikatelé - moje ponuka', '#', 'ne', 'ne', 'ano', 'ano', 'ano'),
(33, 107, 100, 'Zapomenuté heslo', 'login.php?reset', 'ano', 'ne', 'ne', 'ne', 'ne'),
(34, 110, 100, 'Odhlásit se', 'login.php?logout', 'ne', 'ano', 'ano', 'ano', 'ano');

-- --------------------------------------------------------

--
-- Table structure for table `menu_en`
--

CREATE TABLE IF NOT EXISTS `menu_en` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) NOT NULL COMMENT 'Menu order',
  `id_main_menu` int(11) NOT NULL COMMENT 'Id to parent menu',
  `text_menu` varchar(35) NOT NULL,
  `href` varchar(35) NOT NULL COMMENT 'Command',
  `ukaz_vsetci` enum('no','yes') NOT NULL COMMENT 'Menu for all',
  `ukaz_kupujuci` enum('no','yes') NOT NULL COMMENT 'Menu for login users',
  `ukaz_predajca` enum('no','yes') NOT NULL COMMENT 'Menu for company',
  `ukaz_platiaci` enum('no','yes') NOT NULL COMMENT 'Menu for company with pay history',
  `ukaz_admin` enum('no','yes') NOT NULL COMMENT 'Menu for admin',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='English menu' AUTO_INCREMENT=35 ;

--
-- Dumping data for table `menu_en`
--

INSERT INTO `menu_en` (`id`, `id_order`, `id_main_menu`, `text_menu`, `href`, `ukaz_vsetci`, `ukaz_kupujuci`, `ukaz_predajca`, `ukaz_platiaci`, `ukaz_admin`) VALUES
( 1,  1,  0, '', '#', 'yes', 'yes', 'yes', 'yes', 'yes'),
( 2, 10,  0, 'Info', 'info.php', 'yes', 'yes', 'yes', 'yes', 'yes'),
( 3, 11, 10, 'Buyers', 'documents/buyers.html', 'no', 'no', 'yes', 'yes', 'yes'),
( 4, 12, 10, 'Company', 'documents/company.html', 'yes', 'yes', 'yes', 'yes', 'yes'),
( 5, 20,  0, 'Hot Price', '#', 'yes', 'yes', 'yes', 'yes', 'yes'),
( 6, 21, 20, 'Discount', '#', 'yes', 'yes', 'yes', 'yes', 'yes'),
( 7, 22, 20, 'Sale', '#', 'no', 'no', 'yes', 'yes', 'yes'),
( 8, 23, 20, 'Top auction', '#', 'yes', 'yes', 'yes', 'yes', 'yes'),
( 9, 30,  0, 'News', '#', 'yes', 'yes', 'yes', 'yes', 'yes'),
(10, 40,  0, 'Shopping', '#', 'no', 'no', 'yes', 'yes', 'yes'),
(11, 50,  0, 'e-Leaflet', '#', 'yes', 'yes', 'yes', 'yes', 'yes'),
(12, 60,  0, 'Auction', '#', 'no', 'yes', 'yes', 'yes', 'yes'),
(13, 70,  0, 'Music Concerts', 'koncertny_listok.php', 'yes', 'yes', 'yes', 'yes', 'yes'),
(14, 80,  0, 'My offer', '#', 'no', 'no', 'yes', 'yes', 'yes'),
(15, 81, 80, 'Price List', '#', 'no', 'no', 'yes', 'yes', 'yes'),
(16, 82, 80, 'Actualize price list', '#', 'no', 'no', 'yes', 'yes', 'yes'),
(17, 83, 80, 'Import price list', '#', 'no', 'no', 'yes', 'yes', 'yes'),
(18, 84, 80, 'Actualize auctions', '#', 'no', 'no', 'yes', 'yes', 'yes'),
(19, 90,  0, 'Administration', '', 'no', 'no', 'no', 'no', 'yes'),
(20, 91, 90, 'Menu', 'menu.php', 'no', 'no', 'no', 'no', 'yes'),
(21, 92, 90, 'Users texts', 'uziv_texty.php', 'no', 'no', 'no', 'no', 'yes'),
(22, 93, 90, 'Company', 'predajca.php', 'no', 'no', 'no', 'no', 'yes'),
(23, 94, 90, 'Buyers', 'zakaznik.php', 'no', 'no', 'no', 'no', 'yes'),
(24, 95, 90, 'Accepted Payments', 'platby.php', 'no', 'no', 'no', 'no', 'yes'),
(25, 97, 90, 'Administators', 'admin.php', 'no', 'no', 'no', 'no', 'yes'),
(26, 98, 90, 'Documents', 'edit.php', 'no', 'no', 'no', 'no', 'yes'),
(27, 100, 0, 'Login', 'login.php', 'yes', 'yes', 'yes', 'yes', 'yes'),
(28, 101, 100, 'Login', 'login.php', 'yes', 'no', 'no', 'no', 'no'),
(29, 102, 100, 'Registration buyers', 'zakaznik.php?new', 'yes', 'no', 'no', 'no', 'yes'),
(30, 103, 100, 'Registration company', 'predajca.php?new', 'yes', 'no', 'no', 'no', 'yes'),
(31, 104, 100, 'Company - paid services', '#', 'no', 'no', 'no', 'yes', 'yes'),
(32, 105, 100, 'Company - my auction', '#', 'no', 'no', 'yes', 'yes', 'yes'),
(33, 107, 100, 'Reset password', 'login.php?reset', 'yes', 'no', 'no', 'no', 'no'),
(34, 110, 100, 'Logout', 'login.php?logout', 'no', 'yes', 'yes', 'yes', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `predajca`
--

CREATE TABLE IF NOT EXISTS `predajca` (
  `id_predajca` int(11) NOT NULL AUTO_INCREMENT,
  `authorize` tinyint(4) NOT NULL DEFAULT '0',
  `login` varchar(35) NOT NULL,
  `password` varchar(35) NOT NULL,
  `sha1_sald` varchar(28) NOT NULL COMMENT 'Soľ pre posledné prihlásenie',
  `email` varchar(55) NOT NULL,
  `firma` varchar(35) NOT NULL,
  `konatelia` varchar(150) NOT NULL,
  `adresa` varchar(75) NOT NULL,
  `PSC` int(11) NOT NULL,
  `platca` tinyint(1) NOT NULL,
  `spolocne` tinyint(1) NOT NULL,
  `status` int(11) NOT NULL,
  `obchodne_podmienky` longtext NOT NULL,
  `bez_DPH` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_predajca`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci COMMENT='Tabuľka predajcov' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `predajca`
--

INSERT INTO `predajca` (`id_predajca`, `authorize`, `login`, `password`, `sha1_sald`, `email`, `firma`, `konatelia`, `adresa`, `PSC`, `platca`, `spolocne`, `status`, `obchodne_podmienky`, `bez_DPH`) VALUES
(2, 0, 'prvy', 'prvy', 'xpF3HMsy7CyFBpT5PUcpwSb2XQ0=', 'nakup@centrumokien.sk', 'OknaMyjava', 'Jan Siele, Ondrej Kráľ', 'Hlavná 33 Myjava', 8013, 0, 1, 0, 'Obchodné podmienky podľa Lipsum.cz\r\nLákanie vône hulás umýval rohľvekvý a jednovod Marajin a Dobožek rieci Marajin. Dobýva ječnie čo znouvedieť halekamat\r\nPrehľvekv je umraprošť úmyslnko á hafanaďop. Čuvaďiát Ísťkom záprostia úmyvať o býva hulák ačkový Prehľvekv dopary\r\njedosté. Rícipádnie ísť nasy onohuby rojskočár znouvedieť lento časťou bický nasy s. A Dobzor hafan dychvá večný\r\númyvaldeň Hoľvekv umraprošť dopary dechyňský dostičí. Rojskočiar Dobzor Žvesmezil no ačkový umiesi A halekamat\r\nMesíciť s Ňasy. Jedostie.\r\n\r\nNápne o čtie leboto smietkom bo hulákaj bolo k tak Tajakkoľvek. Obzor dievčat nim zteplaleď maľné rí lásna žiari\r\nDostrhnov čuvaď halekamat. Á Depicou ačkový prodený jakošil Mať Niesmetít krádnie čítie Vlačít Ísťkom. Mať Marajin\r\nMať lásny poci ku máš rohľvekvý čnie rícipádnie dostičí. Musí Tajakkoľvek Vény čnie umývať akoto zle premať ním\r\nrohľvekvý a. Ním časťou Tajakkoľvek Vlačít.\r\n\r\nRíklemôt ľahýnkami prodený a Dobzor Dobožek je škovat pôda dostičí poci. Nápne a umýva umraprošť Je zteplaleď bubený\r\nžiari Hoľvekv dychová dráčik. Smut dobýva slochyňsko A autor Ísťkom A spubený v rieci smutomáš. Ječnie nám umraprošť\r\nbický slochyňsko úmyvadlem ja tým rohľvekvý Poľvekvnie alehradi. Čuvaď Tanechreh povodlobi Mať tak Tajakkoľvek\r\nje drátobick.\r\n\r\nDychvá je rojdi Dobožek a umiesi čít k ku smeti smutomáš. Umiesí úmyvadlem spubený úmyvadlem vietkočin a rieci čnie\r\nzásou znouvedieť umývať. Dostičí sudbale mésíčniekl vietorznova čnie zle Lákanie večný ne čnie dobrajin. Smutomáš\r\nprodený ísť dechyňský čtiem hafanaďop ačkový šťastím tým Dobzor Šreziv. Nie večný Tanechreh dychvá Stako smuslušle.\r\n\r\nDobrajin prodený čepraď zvesmezôl s s umýva sudba Dobožek jednovod spubený. Je rúk božný rohľvekvý dopary hudba\r\nbájedpodl úmyvaď obzor nám Žvesmezil. Smut čuvaď ľahýnkami čepraď s umraprošť bický s úmysel božný školný.\r\nRícipádnie Tor boli spoluzavéď Marajin halekamat dobýva zle umiesi psohafanaďop Úmyslušle. Obzor škoľvekvé obačný\r\nmarabubej nim čepraď bo bubený škojdi nie maľné. Škou no.\r\n\r\nUmraprošť umiesí s dobzor ku marabubej časťou dychová vie bo nač. Jednovod Ťmusí Pánova Trilógia málobrazy Záprostie\r\nsobit lento čuvaď jedostie ačkový. Bický ríky Onová s riekomi málobrazy obzor alehradi je Ňatkolek s. Zvesmezôl\r\nbo Čarasný lyžčkou večný premať o maľné Pánova ísťkom Je. Prestá v umýval dopary hudba napné Poľvekvnie bo hudba\r\nbubený ľahýnkami. Ísť ním umýva pretožeto čítie s Prehľvekv riadiť býva začerp.\r\n\r\nJedostie hafanaďop Lákanie Ľakanie dostie s čuvaď Dobožek čnie vietrhnova ísť. Slochyňsko prestie Autnie ísť umýval\r\nRažie Onová liek akoto dychová v. Čuvaleda zásobný časťou bolo Hoľvekv v obačný k nasy lásny umraď. Časťou\r\nslochyňsko bolo dobrajin trilogie Bútny nie obačný roštienkam čuvaleda obzor. Tým večný čapicou parta rojdi\r\nz spubený ním Hoľvekv smietkom o. Žasíčnie riekomi bicie dievčat.\r\n\r\nPa povodlobi Marajin lásna hafan Prehľvekv jakošil lento žiari spubený bájný. Úmyslnko ak Žvesmezil á sudba autor\r\nprestá smutomáš božný školný prodený. Umýva bický obačný dobzor Tor onohuby čnie vônehulás Raďložný čilo rí.\r\nK poci umraprošť nestakkol božný rieci nasy nápne Mesíciť o a. Rí riekomi ríky umraprošť Pánova s krazy čuva\r\nA Ňatkolek bájedpodl. Dostičí riaciť nuchredos lásna rieci.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `uziv_texty`
--

CREATE TABLE IF NOT EXISTS `uziv_texty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tabulka` varchar(35) NOT NULL,
  `stlpec` varchar(70) NOT NULL,
  `poradie` tinyint(4) NOT NULL,
  `popis` varchar(255) NOT NULL,
  `help` varchar(35) NOT NULL,
  `zobrazit` enum('nie','ano') NOT NULL DEFAULT 'ano' COMMENT 'Položka má byť zobrazená',
  `vyzadovat` enum('nie','ano') NOT NULL DEFAULT 'nie' COMMENT 'Má sa vyplnenie tejto položky vyžadovať',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci COMMENT='Užívatelské texty lokalizované' AUTO_INCREMENT=272 ;

--
-- Dumping data for table `uziv_texty`
--

INSERT INTO `uziv_texty` (`id`, `tabulka`, `stlpec`, `poradie`, `popis`, `help`, `zobrazit`, `vyzadovat`) VALUES
(2, 'predajca', 'id_predajca', 0, 'Identifikátor', 'Unikátne číslo predajcu', 'ano', 'nie'),
(3, 'predajca', 'login', 0, 'Užívateľské meno', 'Užívateľské meno', 'ano', 'nie'),
(4, 'predajca', 'password', 0, 'Heslo', 'Heslo pri prihlasovaní', 'ano', 'nie'),
(5, 'predajca', '#', 0, 'Zoznam predajcov', '', 'ano', 'nie'),
(6, 'predajca', 'firma', 0, 'Firma', '', 'ano', 'nie'),
(7, 'predajca', 'konatelia', 0, 'Konateľ firmy', '', 'ano', 'nie'),
(8, 'predajca', 'adresa', 0, 'Adresa firmy', '', 'ano', 'nie'),
(9, 'predajca', 'PSC', 0, 'PSČ', '', 'ano', 'nie'),
(10, 'predajca', 'platca', 0, 'Je plátcom', 'Ak je plátcom DPH vlož 1', 'ano', 'nie'),
(11, 'predajca', 'spolocne', 0, '', '', 'ano', 'nie'),
(12, 'predajca', 'status', 0, 'Status', 'Aký je aktuálny status firmy', 'ano', 'nie'),
(13, 'predajca', 'obchodne_podmienky', 0, 'Obchodné podmienky', '', 'ano', 'nie'),
(14, 'predajca', 'bez_DPH', 0, 'Ceny sú bez DPH', '', 'ano', 'nie'),
(15, 'uziv_texty', 'id', 0, 'Identifikátor', '', 'ano', 'nie'),
(16, 'uziv_texty', 'tabulka', 0, 'Tabuľka', '', 'ano', 'ano'),
(17, 'uziv_texty', 'stlpec', 0, 'Stĺpec', '', 'ano', 'ano'),
(18, 'uziv_texty', 'popis', 0, 'Popis stĺpca pri zobrazovaní', 'Aký text sa zobrazí užívateľovi', 'ano', 'nie'),
(19, 'uziv_texty', 'help', 0, 'Pomocný text', 'Čo sa zobrazí tu', 'ano', 'nie'),
(20, 'uziv_texty', 'zobrazit', 0, 'Zobraziť stĺpec', '', 'ano', 'nie'),
(21, 'uziv_texty', 'vyzadovat', 0, 'Stĺpec má byť vyplnený', 'Ak je nutné tento stĺpec vyplniť', 'ano', 'nie'),
(22, 'uziv_texty', '#', 0, 'Užívateľské texty popisujúce tabuľky pri editácii', '', '', ''),
(106, 'uziv_texty', 'poradie', 0, 'poradie', '', 'ano', 'nie'),
(107, 'uziv_texty_missing', 'TABLE_NAME', 0, 'TABLE_NAME', '', 'nie', ''),
(108, 'uziv_texty_missing', 'COLUMN_NAME', 1, 'COLUMN_NAME', '', 'nie', ''),
(109, 'fio_info', '#', 0, 'Transakcie vo fio banke', '', 'nie', 'nie'),
(110, 'fio_info', 'id_transakce', 1, 'id_transakce', '', 'ano', 'nie'),
(111, 'fio_info', 'cislo', 2, 'cislo', '', 'ano', 'nie'),
(112, 'fio_info', 'mena', 3, 'mena', '', 'ano', 'nie'),
(113, 'fio_info', 'IBAN', 4, 'IBAN', '', 'ano', 'nie'),
(114, 'fio_info', 'kod_banky', 5, 'kod_banky', '', 'ano', 'nie'),
(115, 'fio_info', 'openingBalance', 6, 'openingBalance', '', 'ano', 'nie'),
(116, 'fio_info', 'closingBalance', 7, 'closingBalance', '', 'ano', 'nie'),
(117, 'fio_info', 'dateStart', 8, 'dateStart', '', 'ano', 'nie'),
(118, 'fio_info', 'dateEnd', 9, 'dateEnd', '', 'ano', 'nie'),
(119, 'fio_info', 'idFrom', 10, 'idFrom', '', 'ano', 'nie'),
(120, 'fio_info', 'idTo', 11, 'idTo', '', 'ano', 'nie'),
(121, 'fio_transaction', '#', 0, 'Pohyby na účte vo FIO banke', '', 'ano', 'nie'),
(122, 'fio_transaction', 'id_pohybu', 1, 'Identifikátor', '', 'ano', 'nie'),
(123, 'fio_transaction', 'datum', 2, 'Dátum', '', 'ano', 'nie'),
(124, 'fio_transaction', 'ciastka', 3, 'Čiastka', '', 'ano', 'nie'),
(125, 'fio_transaction', 'mena', 4, 'Mena', '', 'ano', 'nie'),
(126, 'fio_transaction', 'protiucet', 5, 'Protiúčet', '', 'ano', 'nie'),
(127, 'fio_transaction', 'nazev_protiuctu', 6, 'Meno protiúčtu', '', 'nie', 'nie'),
(128, 'fio_transaction', 'banka', 7, 'Banka', '', 'ano', 'nie'),
(129, 'fio_transaction', 'nazev_banky', 8, 'Banka', '', 'nie', 'nie'),
(130, 'fio_transaction', 'KS', 9, 'KS', '', 'nie', 'nie'),
(131, 'fio_transaction', 'VS', 10, 'Variabilný symbol', '', 'ano', 'nie'),
(132, 'fio_transaction', 'SS', 11, 'SS', '', 'nie', 'nie'),
(133, 'fio_transaction', 'popis_interni', 12, 'Popis', '', 'ano', 'nie'),
(134, 'fio_transaction', 'popis', 13, 'Popis', '', 'nie', 'nie'),
(135, 'fio_transaction', 'typ', 14, 'Typ', '', 'ano', 'nie'),
(136, 'fio_transaction', 'provedl', 15, 'Vykonal', '', 'nie', 'nie'),
(137, 'fio_transaction', 'upresneni', 16, 'Upresnenie', '', 'nie', 'nie'),
(138, 'fio_transaction', 'komentar', 17, 'Komentár', '', 'nie', 'nie'),
(139, 'fio_transaction', 'BIC', 18, 'BIC', '', 'ano', 'nie'),
(140, 'fio_transaction', 'ident', 19, 'ident', '', 'ano', 'nie'),
(141, 'zakaznik', '#', 0, 'Zoznam registrovaných zákazníkov', '', 'nie', 'nie'),
(142, 'zakaznik', 'id_zakaznik', 1, 'Idenitfikátor', '', 'ano', 'nie'),
(144, 'zakaznik', 'login', 3, 'Prihlasovacie meno', '', 'ano', 'ano'),
(145, 'zakaznik', 'password', 4, 'Heslo', '', 'ano', 'ano'),
(146, 'zakaznik', 'email', 5, 'email', '', 'ano', 'ano'),
(147, 'zakaznik', 'meno', 6, 'Meno', '', 'ano', 'nie'),
(148, 'zakaznik', 'priezvisko', 7, 'Priezvisko', '', 'ano', 'nie'),
(149, 'zakaznik', 'adresa', 8, 'Adresa', '', 'ano', 'nie'),
(150, 'zakaznik', 'PSC', 9, 'PSČ', '', 'ano', 'nie'),
(151, 'zakaznik', 'bez_DPH', 10, 'Ceny sú bez DPH', '', 'ano', 'nie'),
(152, 'menu', '#', 0, 'Editácia menu podľa skupín užívateľov', '', 'ano', 'nie'),
(153, 'menu', 'id', 1, 'id', '', 'ano', 'nie'),
(154, 'menu', 'id_order', 2, 'Poradie v zobrazení', '', 'ano', 'nie'),
(155, 'menu', 'id_main_menu', 3, 'Id nadradeného menu', '', 'ano', 'nie'),
(156, 'menu', 'text_menu', 4, 'Popisok', '', 'ano', 'nie'),
(157, 'menu', 'ukaz_vsetci', 5, 'Toto menu uvidia aj neprihlásený', '', 'ano', 'nie'),
(158, 'menu', 'ukaz_kupujuci', 6, 'Všetci užívatelia uvidia toto menu', '', 'ano', 'nie'),
(159, 'menu', 'ukaz_predajca', 7, 'Všetci predajcovia uvidia toto menu', '', 'ano', 'nie'),
(160, 'menu', 'ukaz_platiaci', 8, 'Uvidia len platiaci predajcovia', '', 'ano', 'nie'),
(161, 'menu', 'ukaz_admin', 9, 'Určené len pre admina stránky', '', 'ano', 'nie'),
(162, 'predajca', '#', 0, 'Zoznam predajcov', '', 'nie', ''),
(163, 'predajca', 'email', 1, 'email', '', 'ano', 'nie'),
(164, 'admin', '#', 0, 'Administrátori systému', '', 'nie', 'nie'),
(165, 'admin', 'id_admin', 1, 'Identifikátor', '', 'ano', 'nie'),
(167, 'admin', 'login', 3, 'Prihlasovacie meno', '', 'ano', 'ano'),
(168, 'admin', 'password', 4, 'Heslo', '', 'ano', 'ano'),
(169, 'admin', 'sha1_sald', 5, 'Generovaný sald pre posledné prihlásenie', '', 'nie', 'nie'),
(170, 'admin', 'email', 6, 'email', '', 'ano', 'nie'),
(171, 'admin', 'meno', 7, 'Meno', '', 'ano', 'nie'),
(172, 'admin', 'priezvisko', 8, 'Priezvisko', '', 'ano', 'nie'),
(181, 'zakaznik', 'sha1_sald', 1, 'sha1', '', 'nie', 'nie'),
(185, 'menu', 'href', 1, 'PHP príkaz', '', 'ano', 'nie'),
(186, 'predajca', 'sha1_sald', 0, 'Generovaný sald pre posledné prihlásenie', '', 'nie', 'nie'),
(187, 'zakaznik', '#new', 0, 'Registrácia nového zákazníka', '', 'nie', 'nie'),
(188, 'predajca', '#new', 0, 'Registrácia nového predjacu', '', 'nie', 'nie'),
(189, 'admin', '#', 0, 'admin', '', 'ano', 'nie'),
(190, 'admin', 'authorize', 1, 'authorize', '', 'nie', 'nie'),
(220, 'predajca', '#', 0, 'predajca', '', 'ano', 'nie'),
(222, 'zakaznik', '#', 0, 'zakaznik', '', 'ano', 'nie'),
(252, '#web', 'prihlásený', 0, 'Prihlásený', '', 'ano', 'nie'),
(253, '#web', 'aktuálny čas', 0, 'Aktuálny čas', '', 'ano', 'nie'),
(254, '#web', 'užívateľ je odhlásený.', 0, 'Užívateľ je odhlásený.', '', 'ano', 'nie'),
(267, '#web', 'prihlásený %s1', 0, 'Prihlásený %s1', '', 'ano', 'nie'),
(268, '#web', 'registracia na web %s1, potvrd to linkom %s2', 0, 'Práve ste sa registroval na webe %s1\\n Pre ďalšiu prácu musíte potvrdiť registráciu na tomto linku:\\n %s2', '', 'ano', 'nie'),
(269, '#web', 'authorize mail for %s1', 0, 'Authorize mail for %s1', '', 'ano', 'nie'),
(270, '#web', 'na adresu %s1 bol odoslaný autorizačný email.', 0, 'Na adresu %s1 bol odoslaný autorizačný email.', '', 'ano', 'nie'),
(271, '#web', 'nedostatočné oprávnenie', 0, 'Nedostatočné oprávnenie', '', 'ano', 'nie');

-- --------------------------------------------------------

--
-- Stand-in structure for view `uziv_texty_missing`
--
CREATE TABLE IF NOT EXISTS `uziv_texty_missing` (
`TABLE_NAME` varchar(64)
,`COLUMN_NAME` varchar(64)
);
-- --------------------------------------------------------

--
-- Table structure for table `zakaznik`
--

CREATE TABLE IF NOT EXISTS `zakaznik` (
  `id_zakaznik` int(11) NOT NULL AUTO_INCREMENT,
  `authorize` tinyint(4) NOT NULL DEFAULT '0',
  `login` varchar(35) NOT NULL,
  `password` varchar(35) NOT NULL,
  `sha1_sald` varchar(28) NOT NULL COMMENT 'Soľ pre posledné prihlásenie',
  `email` varchar(55) NOT NULL,
  `meno` varchar(35) NOT NULL,
  `priezvisko` varchar(35) NOT NULL,
  `adresa` varchar(75) NOT NULL,
  `PSC` int(11) NOT NULL,
  `bez_DPH` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_zakaznik`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci COMMENT='Tabuľka kupujúcich' AUTO_INCREMENT=49 ;

--
-- Dumping data for table `zakaznik`
--

INSERT INTO `zakaznik` (`id_zakaznik`, `authorize`, `login`, `password`, `sha1_sald`, `email`, `meno`, `priezvisko`, `adresa`, `PSC`, `bez_DPH`) VALUES
(1, 1, 'druhy', 'druhy', 'Cb0RbdMpTMA0n2oc45osjhdTnyg=', '0', 'Jano', 'Janovsky', '', 0, 0),
(2, 0, 'Gudrum', 'heslo', '', 'gudrum@centrum.sk', 'Jasmina', 'Gudrum', '', 0, 0),
(47, 0, 'treti', 'treti', '/g9OfRkfVRWLtHw+zUmYsX4e8z4=', 'etrsek@gmail.com', '', '', '', 0, 0),
(48, 0, 'stvrti', 'stvrti', 'sQxlrVcNv9hR4IeNUsxWs/hESU0=', 'etrsek@gmail.com', '', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Structure for view `uziv_texty_missing`
--
DROP TABLE IF EXISTS `uziv_texty_missing`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `uziv_texty_missing` AS select `information_schema`.`columns`.`TABLE_NAME` AS `TABLE_NAME`,`information_schema`.`columns`.`COLUMN_NAME` AS `COLUMN_NAME`,`information_schema`.`columns`.`COLUMN_COMMENT` AS `COLUMN_COMMENT` from `information_schema`.`columns` where ((`information_schema`.`columns`.`TABLE_SCHEMA` = 'sekerakeu') and (not(concat(`information_schema`.`columns`.`TABLE_NAME`,'-',`information_schema`.`columns`.`COLUMN_NAME`) COLLATE utf8_slovak_ci in (select concat(`sekerakeu`.`uziv_texty`.`tabulka`,'-',`sekerakeu`.`uziv_texty`.`stlpec`) from `sekerakeu`.`uziv_texty`))));

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
