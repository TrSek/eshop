<?php

/**
 * Slovenská lokalizácia
 * 
 */


$this->text['Add_Record'] = 'Nový záznam';
$this->text['Clear_search'] = 'Zruš filtr';
$this->text['Search'] = 'Filtr';

$this->text['Go_back'] = 'Zpátky';
$this->text['Save'] = 'Uložit';
$this->text['saved'] = 'Uloženo';
$this->text['Delete'] = 'Smazat';
$this->text['deleted'] = 'Smazáno';

$this->text['Previous'] = 'Predešlý';
$this->text['Next'] = 'Nasledující';

$this->text['Nothing_found'] = 'Nenašel jsem nic';
$this->text['Check_the_required_fields'] = 'Zkontrolujte požadované políčka (označené žlutou)';
$this->text['Protect_this_directory_with'] = 'Zapni ochranu tohoto adresáře s pomocí ';

?>
