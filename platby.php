<?php
require_once("db/config.php");
require_once("db/mte/mte.php");
require_once("fio/update.php");
$tabledit = new MySQLtabledit();

# database settings:
$tabledit->database_connect_quick(_DB_DATABASE, _DB_HOST, _DB_USER, _DB_PASSWORD, $user->language, 'fio_transaction');

# potreba pripojit sa a aktualizovat DB
if( $_REQUEST['connect'] == 'fio')
	transakcie_update_fio();

$tabledit->insert_button("#", $localize->text("Aktualizuj vo FIO"), "connect=fio");
$tabledit->set_read_only(true);
$tabledit->database_connect();
$tabledit->do_it( basename(__FILE__));
$tabledit->database_disconnect();
?>
