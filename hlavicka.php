<?php
  session_start();
  
  require_once("./zdeno/time_meas.php");
  require_once("./db/config.php");
  require_once("./language.php");
  require_once("./localize.php");
  require_once("./login/LoginApi.php");
  
  $localize = new LocalizeApi(CheckLanguage());
  $user = new LoginApi(_DB_DATABASE, _DB_HOST, _DB_USER, _DB_PASSWORD, SESSION_TIMEOUT, $localize->language);
  $user->Authorize();

  if( isset($_REQUEST['debug']) || isset($_GET['debug'])) 
  {
  	echo "<br><br><br>";
	var_dump($GLOBALS);
  }
?>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta name="author" content="Zdeno Sekerák">
	<meta name="created" content="20081006;13165940">
	<link rel="stylesheet" type="text/css" media="screen" href="css/menu.css" />
	<link rel="stylesheet" type="text/css" media="print"  href="css/menu_print.css" />
</head>

<style>
.logo2 {position: absolute; top: 42px; left: 0px; width: 100%; height: 28px; background-image: url("res/menu_podklad.jpg");}
</style>

<div class='print_css'>
<div class='logo2'></div>
<div style='top:43px; position:absolute;'>
	<?php
	require_once("menubuild.php");
	?>
</div>
</div>	
<body background='res/linen-fine.jpg' bgproperties='fixed' dir='ltr' lang='cs-cz' vlink=#0000ff id='bdy'>
<div align='right'>
	<?php
	foreach ($languages as $lang)
	echo "<a href='?lang=$lang'>$lang</a> ";
	?>
</div>